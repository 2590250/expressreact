import { DataGrid } from '@mui/x-data-grid';
import { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import Form from './components/Form';

const columns = [
  { field: 'id', headerName: 'ID', width: 70 },
  { field: 'firstName', headerName: 'First name', width: 130 },
  { field: 'lastName', headerName: 'Last name', width: 130 },
  {
    field: 'age',
    headerName: 'Age',
    type: 'number',
    width: 90,
  },
  {
    field: 'fullName',
    headerName: 'Full name',
    description: 'This column has a value getter and is not sortable.',
    sortable: false,
    width: 160,
    valueGetter: (params) =>
      `${params.row.firstName || ''} ${params.row.lastName || ''}`,
  },
];

export default function DataTable() {
  const [rows, setRows] = useState([]);
  const [variable1, setVariable1] = useState(1);
  const [receivedData, setReceivedData] = useState(1);
  const isInitialRender = useRef(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get('/get-data');

        if (Array.isArray(response.data)) {
          setRows(response.data);
        } else {
          console.error('Data is in an unexpected format:', response.data);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, [variable1]);

 useEffect(() => {
    if (!isInitialRender.current) {
      setRows(receivedData);
    } else {
      isInitialRender.current = false;
    }
  }, [receivedData]);

  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 5 },
          },
        }}
        pageSizeOptions={[5, 10]}
        checkboxSelection
      />
      <Form
        variable1={variable1}
        setVariable1={setVariable1}
        setReceivedData={setReceivedData}
      />
    </div>
  );
}
