import React, { useState } from 'react';
import axios from 'axios';

function FormInput ({ form, setForm, row, property, field, keyword1 }) {
  return (
    <input
        value= { property }
        onChange= { (e) => change(form, setForm, row.rowNumber, field, e.target.value, e) }
        type = { keyword1 }
    />
  );
};
function change (form, setForm, rowNumber, field, value, e) {
  e.preventDefault();
  const update = form.map((row) =>
    row.rowNumber === rowNumber ? { ...row, [field]: value } : row
  );
  setForm(update);
};
function Form({ variable1, setVariable1, setReceivedData, change }) {
  const [newForm, setNewForm] = useState([{ rowNumber: 1, lastName: '', firstName: '', age: '' }]);
  const [updateForm, setUpdateForm] = useState([{ rowNumber: 1, id: '', lastName: '', firstName: '', age: '' }]); 
  const [deleteForm, setDeleteForm] = useState([{ rowNumber: 1, id: ''}]);
  const [searchID, setSearchID] = useState({id: ''});
  const [searchFirstName, setSearchFirstName] = useState({firstName: ''});
  const [searchLastName, setSearchLastName] = useState({lastName: ''});
  const [searchAge, setSearchAge] = useState({age: ''});
  // Reset 
  const reset = (form, setForm) => {
    const updatedForm = { ...form[0] };
    for (const property in updatedForm) {
      if (property === "rowNumber") {
        updatedForm[property] = 1;
      } else {
        updatedForm[property] = '';
      }
    }
    setForm([updatedForm]);
  };
  // Add row
  const addRow = (form, setForm) => {
  const updatedForm = [...form];
  const newRow = { ...updatedForm[updatedForm.length - 1], rowNumber: updatedForm.length + 1 };
  updatedForm.push(newRow);
  setForm(updatedForm);
  };
  // Remove row
  const removeRow = (form, setForm) => {
    if (form.length > 1) {
      const update = form.slice(0, -1);
      setForm(update);
    }
  };
  // Update
  const submit = async (url, form, setForm, method, e) => { 
    e.preventDefault();
    if (form.some(row => Object.values(row).some(value => value == null || value === ''))) {
      console.error('Form contain null or empty values. Form submission stopped.');
      return;
    }
    try {
      let response;
      switch (method) {
        case 'post':
          response = await axios.post(url, { form }, { headers: { 'Content-Type': 'application/json' },});
          break;
        case 'put':
          response = await axios.put(url, { form }, { headers: { 'Content-Type': 'application/json' },});
          break;
        case 'delete':
          response = await axios.delete(url, { data: { form }, headers: { 'Content-Type': 'application/json' },});
          break;
        default:
          console.error(`Invalid method: ${method}`);
          return;
      }
      console.log(`${method.toUpperCase()} Form Response:`, response);
      if (response && response.status === 200) {
        console.log(`Data successfully ${method === 'post' ? 'sent to' : method === 'put' ? 'updated on' : 'deleted from'} the server`);
        reset(form, setForm);
        setVariable1((prev) => prev + 1);
      } else {
        console.error(`Failed to ${method === 'post' ? 'send data to' : method === 'put' ? 'update data on' : 'delete data from'} the server`);
      }
    } catch (error) {
      console.error(`Error ${method === 'post' ? 'sending data to' : method === 'put' ? 'updating data on' : 'deleting data from'} the server:`, error);
    }
  };
  // Filter
  const search = async (form, searchType, e) => {
    e.preventDefault();
    try {
      const data = form[searchType];
      const response = await axios.get('/search-data', {
        params: { data },
        headers: {
          'Content-Type': 'application/json',
          'type': searchType,
        },
      });
      console.log(`Search ${searchType} Form Response:`, response);
      console.log('Data:', response.data);
      setReceivedData(response.data);
    } catch (error) {
      console.error(`Error searching data by ${searchType}:`, error);
    }
  };
  // Delete
  const deleteTable = async () => {
  try {
    await axios.delete('/delete-All-Data');
    console.log('All data deleted successfully.');
    setVariable1(variable1 + 1);
  } catch (error) {
    console.error('Error deleting all data:', error);
  }
  };
  // Save
  const saveTable = async () => {
    try {
    await axios.post('/save-data');
    console.log('Data saved successfully.');
    setVariable1(variable1 + 1);
    } catch (error) {
    console.error('Error saving data:', error);
    }
  };
  return ( 
  <div>
  <h5> Add new rows </h5>
  <form onSubmit={(e) => submit('/send-data', newForm, setNewForm, 'post', e)}>
    <div>
      {newForm.map((row) => (
        <div key={row.rowNumber}>
          <label> Last Name </label>
          <FormInput form={newForm} setForm={setNewForm} row={row} property={row.firstName} field='firstName' keyword1="text" />;
          <label> First Name </label>
          <FormInput form={newForm} setForm={setNewForm} row={row} property={row.lastName} field='lastName' keyword1="text" />;
          <label> Age </label>
          <FormInput form={newForm} setForm={setNewForm} row={row} property={row.age} field='age' keyword1="number" />;
        </div>
      ))}
    </div>
    <button type="button" onClick={() => addRow(newForm, setNewForm)}> Add Form Row </button>
    <button type="button" onClick={() => removeRow(newForm, setNewForm)}> Remove Row </button>
    <button type="submit">Submit</button>
  </form>

  <h5>Update rows</h5>
  <form onSubmit={(e) => submit('/update-data', updateForm, setUpdateForm, 'put', e)}>
    <div>
      {updateForm.map((row) => (
        <div key={row.rowNumber}>
          <label>ID</label>
          <FormInput form={updateForm} setForm={setUpdateForm} row={row} property={row.id} field='id' keyword1="number" />;
          <label>First Name</label>
          <FormInput form={updateForm} setForm={setUpdateForm} row={row} property={row.firstName} field='firstName' keyword1="text" />;
          <label>Last Name</label>
          <FormInput form={updateForm} setForm={setUpdateForm} row={row} property={row.lastName} field='lastName' keyword1="text" />;
          <label>Age Name</label>
          <FormInput form={updateForm} setForm={setUpdateForm} row={row} property={row.age} field='age' keyword1="number" />;
        </div>
      ))}
    </div>
    <button type="button" onClick={() => addRow(updateForm, setUpdateForm)}> Add Form Row </button>
    <button type="button" onClick={() => removeRow(updateForm, setUpdateForm)}> Remove Row </button>
    <button type="submit">Submit</button>
  </form>

  <h5>Search rows</h5>
  <form onSubmit={(e) => search(searchID, 'id', e)}>
    <label>ID</label>
    <input type="number" value={searchID.id} onChange={(e) => setSearchID({ ...searchID, id: e.target.value })}/>
    <button type="submit">Search</button>
  </form>
  <form onSubmit={(e) => search(searchFirstName, 'firstName', e)}>
    <label>First Name</label>
    <input type="text" value={searchFirstName.firstName} onChange={(e) => setSearchFirstName({ ...searchFirstName, firstName: e.target.value })}/>
    <button type="submit">Search</button>
  </form>
  <form onSubmit={(e) => search(searchLastName, 'lastName', e)}>
    <label>Last Name</label>
    <input type="text" value={searchLastName.lastName} onChange={(e) => setSearchLastName({ ...searchLastName, lastName: e.target.value })}/>
    <button type="submit">Search</button>
  </form>
  <form onSubmit={(e) => search(searchAge, 'age', e)}>
    <label>Age</label>
    <input type="number" value={searchAge.age} onChange={(e) => setSearchAge({ ...searchAge, age: e.target.value })}/>
    <button type="submit">Search</button>
  </form>

  <h5>Delete rows</h5>
  <form onSubmit={(e) => submit('/delete-data', deleteForm, setDeleteForm, 'delete', e)}>
    <div>
      {deleteForm.map((row) => (
        <div key={row.rowNumber}>
          <label>ID</label>
          <FormInput form={deleteForm} setForm={setDeleteForm} row={row} property={row.id} field='id' keyword1="number" />;
        </div>
      ))}
    </div>
    <button type="button" onClick={() => addRow(deleteForm, setDeleteForm)}> Add Form Row </button>
    <button type="button" onClick={() => removeRow(deleteForm, setDeleteForm)}> Remove Row </button>
    <button type="submit">Delete</button>
  </form>
  <button type="button" onClick={deleteTable}> Delete All Data </button>
  <button type="button" onClick={saveTable}> Save </button>
</div>
)}
export default Form;