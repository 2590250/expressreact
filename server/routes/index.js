const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();
const bodyParser = require('body-parser');
const path = require('path');
const { stringify } = require('querystring');

const dbPath = path.resolve(__dirname, '../database.db'); //Absolute path
const db = new sqlite3.Database(dbPath);

const jsonParser = bodyParser.json();

// Create the "dataTable" table if it does not exist
db.run(`CREATE TABLE IF NOT EXISTS "dataTable" (
  id INTEGER PRIMARY KEY,
  lastName TEXT,
  firstName TEXT,
  age INTEGER
)`, (err) => {
  if (err) {
    console.error('Error creating table:', err);
  } else {
    console.log('Table created or already exists');
  }
});

// Create the "dataTableTemporary" table if it does not exist
db.run(`CREATE TABLE IF NOT EXISTS "dataTableTemporary" (
  id INTEGER PRIMARY KEY,
  lastName TEXT,
  firstName TEXT,
  age INTEGER
)`, (err) => {
  if (err) {
    console.error('Error creating table:', err);
  } else {
    console.log('Table created or already exists');
  }
});

db.serialize(() => {
  db.run('DELETE FROM "dataTableTemporary"', (deleteErr) => {
    if (deleteErr) {
      console.error('Error deleting data from dataTableTemporary:', deleteErr.message);
    }

    db.run('INSERT INTO "dataTableTemporary" SELECT * FROM "dataTable"', (insertErr) => {
      if (insertErr) {
        console.error('Error copying data from dataTable to dataTableTemporary:', insertErr.message);
      }
    });
  });
});

router.post('/save-data', (req, res) => {
  db.serialize(() => {
    db.run('DELETE FROM "dataTable"', (err) => {
      if (err) {
        console.error('Error deleting data from dataTable:', err);
        return res.status(500).json({ error: 'Internal Server Error' });
      }
    });
    db.run('INSERT INTO "dataTable" SELECT * FROM "dataTableTemporary"', (err) => {
      if (err) {
        console.error('Error copying data from dataTableTemporary to dataTable:', err);
        return res.status(500).json({ error: 'Internal Server Error' });
      }

      res.status(200).json({ message: 'Data saved successfully' });
    });
  });
});

// Define a route to handle the user's input
router.post('/send-data', jsonParser, (req, res) => {
  const jsonData = req.body;

  if (!jsonData.form || !Array.isArray(jsonData.form)) {
    console.log('Invalid JSON format:', jsonData);
    return res.status(400).json({ error: 'Invalid JSON format' });
  }

  db.serialize(() => {
    jsonData.form.forEach((row) => {
      const placeholders = ['?', '?', '?'].join(',');
      const values = [row.lastName, row.firstName, row.age];

      db.run(`INSERT INTO "dataTableTemporary" ("lastName", "firstName", "age") VALUES (${placeholders})`, values, (err) => {
        if (err) {
          console.error('Error inserting data into the database:', err);
        }
      });
    });

    console.log('Data inserted successfully:', jsonData.form);
    res.status(200).json({ message: 'Data inserted successfully' });
  });
});

// Define a route to handle updating the user's input
router.put('/update-data', jsonParser, (req, res) => {
  const jsonData = req.body;

  if (!jsonData.form || !Array.isArray(jsonData.form)) {
    console.log('Invalid JSON format:', jsonData);
    return res.status(400).json({ error: 'Invalid JSON format' });
  }

  db.serialize(() => {
    jsonData.form.forEach((row) => {
      const values = [row.lastName, row.firstName, row.age, row.id];

      db.run(
        `UPDATE "dataTableTemporary" SET "lastName" = ?, "firstName" = ?, "age" = ? WHERE "id" = ?`,
        values,
        (err) => {
          if (err) {
            console.error('Error updating data in the database:', err);
          }
        }
      );
    });

    console.log('Data updated successfully:', jsonData.form);
    res.status(200).json({ message: 'Data updated successfully' });
  });
});

// Define a route to retrieve and send data from the 'dataTableTemporary' table
router.get('/get-data', (req, res) => {
  // Query the 'dataTableTemporary' table and convert the result to JSON
  db.all('SELECT * FROM "dataTableTemporary"', (err, form) => {
    if (err) {
      console.error('Error querying the database:', err);
      return res.status(500).json({ error: 'Internal Server Error' });
    }

    console.log('Data queried successfully:', form);
    // Send the data as JSON in the response
    res.json(form);
  });
});

router.delete('/delete-data', jsonParser, (req, res) => {
  const jsonData = req.body;

  console.log('Received delete request with data:', jsonData);

  db.serialize(() => {
    const IDArray = jsonData.form.map(row => parseInt(row.id));

    IDArray.forEach((row) => {
      db.run(
        'DELETE FROM dataTableTemporary WHERE id = ?',
        IDArray,
        (err) => {
          if (err) {
            console.error('Error deleting data from the database:', err);
          } else {
            console.log('Data deleted successfully for ID:', IDArray);
          }
        }
      );
    });

    console.log('All data deleted successfully:', IDArray);
    res.status(200).json({ message: 'Data deleted successfully' });
  });
});

// Define a route to handle deleting all data from the 'dataTableTemporary' table
router.delete('/delete-all-data', jsonParser, (req, res) => {
  db.run('DELETE FROM "dataTableTemporary"', (err) => {
    if (err) {
      console.error('Error deleting all data from the database:', err);
      return res.status(500).json({ error: 'Internal Server Error' });
    }
  res.status(200).json({ message: 'Data deleted successfully' });
  });
});




// Define a route to handle filtering requests
router.get('/search-data', jsonParser, (req, res) => {

  let data = req.query.data;
  let type = req.headers.type;
  let sqlQuery;

  console.log(data);
  console.log(type);

  if (type === 'age' || type === 'id') {
    sqlQuery = `SELECT * FROM "dataTableTemporary" WHERE ${type} = ?`;
  } else {
    sqlQuery = `SELECT * FROM "dataTableTemporary" WHERE ${type} LIKE ?`;
    data = `%${data}%`;
  }

  db.all(sqlQuery, [data], (err, form) => {
    if (err) {
      console.error('Error executing query:', err);
      return res.status(500).json({ error: 'Internal Server Error' });
    }
    console.log('Search results:', form);
    res.json(form);
  });
});

module.exports = router;







